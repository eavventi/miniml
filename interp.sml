(* From: M.Tofte - Essentials of Standard ML Modules *)

signature INTERPRETER =
sig
    val interpret : string -> string
    val eval : bool ref
    and tc : bool ref
end;

(* Syntax *)

signature EXPRESSION =
sig
    datatype Expression =
	     SUMexpr of Expression * Expression |
	     DIFFexpr of Expression * Expression |
	     PRODexpr of Expression * Expression |
	     BOOLexpr of bool |
	     EQexpr of Expression * Expression |
	     CONDexpr of Expression * Expression * Expression |
	     LISTexpr of Expression list |
	     DECLexpr of string * Expression * Expression |
	     RECDECLexpr of string * Expression * Expression |
	     IDENTexpr of string |
	     LAMBDAexpr of string * Expression |
	     NUMBERexpr of int
end;

(* Parsing *)

signature PARSER =
sig
    structure E : EXPRESSION

    exception Lexical of string
    exception Syntax of string

    val parse : string -> E.Expression
end;

(* Environments *)

signature ENVIRONMENT =
sig
    type 'object Environment

    exception Retrieve of string

    val emptyEnv : 'object Environment
    val declare : string * 'object * 'Object Environment ->
		  'object Environment
    val retrieve : string * 'object Environment -> 'onject
end;

(* Evaluation *)

signature VALUE =
sig
    type Value
    exception Value

    val mkValueNumber : int -> Value
    and unValueNumber : Value -> int

    val mkValueBool : bool -> Value
    and unValueBool : Value -> bool

    val ValueNil : Value

    val mkValueCons : Value * Value -> Value
    and unValueHead : Value -> Value
    and unValueTail : Value -> Value

    val eqValue : Value * Value -> bool
    val printValue : Value -> string
end;

signature EVALUATOR =
sig
    structure Exp : EXPRESSION
    structure Val : VALUE
    exception Unimplemented
    val evaluate : Exp.Expression -> Val.Value
end;

(* Type checking *)

signature TYPE =
sig
    type Type

    exception Type
    val mkTypeInt : unit -> Type
    and unTypeInt : Type -> unit

    val mkTypeBool : unit -> Type
    and unTypeBool : Type -> unit

    val prType : Type -> string
end;

signature TYPECHECKER =
sig
    structure Exp : EXPRESSION
    structure Type : TYPE
    exception NotImplemented of string
    exception TypeError of Exp.Expression * string
    val typecheck : Exp.Expression -> Type.Type
end;

(* The interpreter *)

functor Interpreter
	    (structure Ty : TYPE
	     structure Value : VALUE
	     structure Parser : PARSER
	     structure TyCh : TYPECHECKER
	     structure Evaluator : EVALUATOR
	     sharing Parser.E = TyCh.Exp = Evaluator.Exp
		     and TyCh.Type = Ty
		     and Evaluator.Val = Value
	    ) : INTERPRETER =
struct

(* isn't it better to have a true toggle? this  *)
    val eval = ref false
    and tc = ref true

    fun interpret(str) =
      let val abstsyn = Parser.parse str
	  val typestr = if !tc
			then Ty.prType(TyCh.typecheck abstsyn)
			else "(disabled)"
	  val valuestr = if !eval
			 then Value.printValue(Evaluator.evaluate abstsyn)
			 else "(disabled)"

      in valuestr ^ " : " ^ typestr
      end
      handle Evaluator.Unimplemented =>
	     "Evaluator not fully implemented"
	   | TyCh.NotImplemented msg =>
	     "Type Checker not fully implemented " ^ msg
	   | Value.Value => "Run-time error"
	   | Parser.Syntax msg => "Syntax Error: " ^ msg
	   | Parser.Lexical msg => "Lexical Error: " ^ msg
	   | TyCh.TypeError(_,msg) => "Type Error: " ^ msg 
end;

(* the Evaluator *)

functor Evaluator
	    (structure Expression : EXPRESSION
	     structure Value : VALUE) : EVALUATOR =
struct
    structure Exp = Expression
    structure Val = Value
    exception Unimplemented

    local
	open Expression Value
	fun evaluate exp =
	  case exp of
	      BOOLexpr b => mkValueBool b
	    | NUMBERexpr i => mkValueNumber i
	    | SUMexpr(e1, e2) =>
	      let val e1' = evaluate e1
		  val e2' = evaluate e2
	      in
		  mkValueNumber(unValueNumber e1' +
				unValueNumber e2')
	      end
	    | DIFFexpr(e1, e2) =>
	      let val e1' = evaluate e1
		  val e2' = evaluate e2
	      in
		  mkValueNumber(unValueNumber e1' -
				unValueNumber e2')
	      end
	    | PRODexpr(e1, e2) =>
	      let val e1' = evaluate e1
		  val e2' = evaluate e2
	      in
		  mkValueNumber(unValueNumber e1' *
				unValueNumber e2')
	      end
	    | EQexpr _ => raise Unimplemented
	    | CONDexpr _ => raise Unimplemented
	    (*| CONSexpr _ => raise Unimplemented*)
	    | LISTexpr _ => raise Unimplemented
	    | DECLexpr _ => raise Unimplemented
	    | RECDECLexpr _ => raise Unimplemented
	    | IDENTexpr _ => raise Unimplemented
	    | LAMBDAexpr _ => raise Unimplemented
	    (*| APPLexpr _ => raise Unimplemented*)
    in
    val evaluate = evaluate
    end
end;

(* the type checker *)

functor TypeChecker
	    (structure Ex : EXPRESSION
	     structure Ty : TYPE) =
struct
    structure Exp = Ex
    structure Type = Ty
    exception NotImplemented of string
    exception TypeError of Ex.Expression * string

    fun tc (exp : Ex.Expression) : Ty.Type =
      case exp of
	  Ex.BOOLexpr b => raise NotImplemented "(bool const)"
	| Ex.NUMBERexpr _ => Ty.mkTypeInt ()
	| Ex.SUMexpr (e1, e2) => checkIntBin (e1, e2)
	| Ex.DIFFexpr (e1, e2) => checkIntBin (e1, e2)
	| Ex.PRODexpr (e1, e2) => checkIntBin (e1, e2)
	| Ex.LISTexpr _ => raise NotImplemented "(lists)"
	| Ex.EQexpr _ => raise NotImplemented "(equality)"
	| Ex.CONDexpr _ => raise NotImplemented "(conditional)"
	| Ex.DECLexpr _ => raise NotImplemented "(declaration)"
	| Ex.RECDECLexpr _ => raise NotImplemented "(rec decl)"
	| Ex.IDENTexpr _ => raise NotImplemented "(identifier)"
	| Ex.LAMBDAexpr _ => raise NotImplemented "(application)" 

    and checkIntBin (e1, e2) =
	let val t1 = tc e1
	    val _ = Ty.unTypeInt t1
		    handle Ty.Type => raise TypeError(e1,
						      "expected int")
	    val t2 = tc e2
	    val _ = Ty.unTypeInt t2
		    handle Ty.Type => raise TypeError(e2,
						      "expected int")
	in Ty.mkTypeInt ()
	end;

    val typecheck = tc

end;
					    
